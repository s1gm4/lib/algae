# Algae

C++ Library for Linear Algebra.

Vector and Matrix standard operations.

## Installation

```bash
git clone git@framagit.org:1ibre/lib/algae.git
cd algae
```

## Usage
```cpp
// ./src/main.cpp
#include <iostream>
#include "./path/to/Matrix.hpp"

// ...
Matrix<int> m(2, 2);
m.set(1, 1, 2) // Set component (2, 2) to value 2
std::cout << m.at(1, 1) << std::endl;
std::cout << m.toString() << std::endl;
// ...
```

```bash
g++ ./src/main.cpp -lm
```

## Authors

Made with ♥ by Samuel ORTION.

## Contributing

Pull requests are welcome.

## License

This work is licensed under the MIT License.