# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`Matrix`](#classMatrix) | 

# class `Matrix` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`Matrix`](#classMatrix_1a9d567e3a121b1be0c3f9c461cab524fe)`()` | Construct a new [Matrix](#classMatrix) object (empty)
`public inline  `[`Matrix`](#classMatrix_1a7957611671a8a00be7cfb7494bf81f8c)`(unsigned int m,unsigned int n)` | Construct a new [Matrix](#classMatrix) object.
`public inline  `[`Matrix`](#classMatrix_1a7ae838c239f3fc7c0527c7c33cc6e74b)`(T * data,unsigned int n,unsigned int m,bool byrow)` | Construct a new [Matrix](#classMatrix) object from a 2D array.
`public inline  `[`Matrix`](#classMatrix_1a78e9753133cbddef9fb6d73fab16c655)`(const `[`Matrix`](#classMatrix)` & source)` | Construct a new [Matrix](#classMatrix) object from an other matrix.
`public inline  `[`~Matrix`](#classMatrix_1a91aa704de674203e96aece9e1955ccd3)`()` | Destroy the [Matrix](#classMatrix) object.
`public inline T `[`at`](#classMatrix_1a052b031c68f9c21bc550f71049f84b0e)`(unsigned int i,unsigned int j)` | Get matrix component at given indices.
`public inline T * `[`row`](#classMatrix_1a6acc450306181314946a3d4d75cf16f0)`(unsigned int j)` | Get the row of the matrix at given index.
`public inline T * `[`col`](#classMatrix_1a1418afcd4b8966508614e68534c8be0f)`(unsigned int i)` | Get the column of the matrix at given index.
`public inline void `[`set`](#classMatrix_1a23611f8052262ff8d271fc6760b6828d)`(unsigned int i,unsigned int j,T value)` | Set value of a component at give indices.
`public inline std::string `[`toString`](#classMatrix_1a6a862549622c26523ad3a9d5263e0574)`()` | Get string representation of the matrix.
`public inline bool `[`operator==`](#classMatrix_1ab1a7748c0ac6dbf3f63c927f9db783a9)`(const `[`Matrix`](#classMatrix)` & other) const` | Compare two matrices.
`public inline bool `[`operator!=`](#classMatrix_1af0b16242406da9f8a849ea8661f58e73)`(const `[`Matrix`](#classMatrix)` & other) const` | Check if two matrices are not equal.
`public inline `[`Matrix`](#classMatrix)` `[`operator=`](#classMatrix_1a39d1492eed5d4334f3056553f7196c62)`(const `[`Matrix`](#classMatrix)` & source)` | Assign a matrix to another matrix.
`public inline `[`Matrix`](#classMatrix)` `[`operator+`](#classMatrix_1a9cc64309ac456e4dc948eedee3263e32)`(const `[`Matrix`](#classMatrix)` & other)` | Add two matrices.
`public inline `[`Matrix`](#classMatrix)` `[`operator+`](#classMatrix_1a26494989cf1ab504cacb48ab6115bfef)`(const T scalar)` | Add a scalar to a matrix.
`public inline `[`Matrix`](#classMatrix)` `[`operator-`](#classMatrix_1a1ccb00bdfd32d7907e0601dcd852abb4)`(const `[`Matrix`](#classMatrix)` & other)` | Subtract two matrices.
`public inline `[`Matrix`](#classMatrix)` `[`operator-`](#classMatrix_1a561ebf4355f56fd22891dd15eac2edbd)`(const T scalar)` | Subtract a scalar from a matrix.
`public inline `[`Matrix`](#classMatrix)` `[`operator/`](#classMatrix_1ae69b53b5925bb653c6916a7fbc42cc69)`(const T scalar)` | Divide a matrix by a scalar.
`public inline `[`Matrix`](#classMatrix)` `[`operator*`](#classMatrix_1a7b1da650e3d336cebea80b32aa89d4e1)`(const `[`Matrix`](#classMatrix)` & other)` | Multiply two matrices.
`public inline `[`Matrix`](#classMatrix)` `[`operator*`](#classMatrix_1a31550583c457a4334e667be98e391e15)`(const T scalar)` | Scale a matrix by a scalar.
`public inline void `[`scale`](#classMatrix_1a1cfcb8902f2ffee566a80737b1df976a)`(T scalar)` | Scale the matrix by a scalar.
`public inline void `[`apply`](#classMatrix_1a388b917cbc5e2c1549f3ae70bcd70b55)`(T(* T)` | Apply a function for each component of the matrix
`public inline `[`Matrix`](#classMatrix)` `[`transpose`](#classMatrix_1a0909615f00692428503e3b9357e22649)`()` | Get the transpose of the matrix.
`public inline `[`Matrix`](#classMatrix)` `[`minor`](#classMatrix_1a0bc7a1822815573004c80d99b7c3fcdd)`(unsigned int a,unsigned int b)` | Get the minor matrix.
`public inline `[`Matrix`](#classMatrix)` `[`cofactor`](#classMatrix_1a422a7e24e38811a6eb8d88872cae92b8)`()` | Compute the cofactor of the matrix.
`public inline `[`Matrix`](#classMatrix)` `[`adjoint`](#classMatrix_1a495442a4403135e9983b5339366c98e9)`()` | Compute the adjoin of the matrix.
`public inline `[`Matrix`](#classMatrix)` `[`inv`](#classMatrix_1a153f654e832e957067d1732c3e234f06)`()` | Inverse of a matrix.
`public inline T `[`determinant`](#classMatrix_1a13c1e1219da7b07cb0655c7a5dfaa88e)`()` | Compute the determinant of the matrix using cofactor method.
`public inline `[`Matrix`](#classMatrix)` `[`copy`](#classMatrix_1a215ee9de53b8648447755ed0b74f40ce)`()` | Get a copy of the matrix.

## Members

#### `public inline  `[`Matrix`](#classMatrix_1a9d567e3a121b1be0c3f9c461cab524fe)`()` 

Construct a new [Matrix](#classMatrix) object (empty)

#### `public inline  `[`Matrix`](#classMatrix_1a7957611671a8a00be7cfb7494bf81f8c)`(unsigned int m,unsigned int n)` 

Construct a new [Matrix](#classMatrix) object.

#### Parameters
* `m` number of rows 

* `n` number of columns

#### `public inline  `[`Matrix`](#classMatrix_1a7ae838c239f3fc7c0527c7c33cc6e74b)`(T * data,unsigned int n,unsigned int m,bool byrow)` 

Construct a new [Matrix](#classMatrix) object from a 2D array.

#### Parameters
* `data` 2D array of type T 

* `n` number of rows 

* `m` number of columns 

* `byrow` specify if the data is given by row or by column

#### `public inline  `[`Matrix`](#classMatrix_1a78e9753133cbddef9fb6d73fab16c655)`(const `[`Matrix`](#classMatrix)` & source)` 

Construct a new [Matrix](#classMatrix) object from an other matrix.

#### Parameters
* `source` [Matrix](#classMatrix) to copy

#### `public inline  `[`~Matrix`](#classMatrix_1a91aa704de674203e96aece9e1955ccd3)`()` 

Destroy the [Matrix](#classMatrix) object.

#### `public inline T `[`at`](#classMatrix_1a052b031c68f9c21bc550f71049f84b0e)`(unsigned int i,unsigned int j)` 

Get matrix component at given indices.

#### Parameters
* `i` column index 

* `j` row index 

#### Returns
T value at given indices

#### `public inline T * `[`row`](#classMatrix_1a6acc450306181314946a3d4d75cf16f0)`(unsigned int j)` 

Get the row of the matrix at given index.

#### Parameters
* `j` row index 

#### Returns
T* row at given index

#### `public inline T * `[`col`](#classMatrix_1a1418afcd4b8966508614e68534c8be0f)`(unsigned int i)` 

Get the column of the matrix at given index.

#### Parameters
* `i` column index 

#### Returns
T* column at given index

#### `public inline void `[`set`](#classMatrix_1a23611f8052262ff8d271fc6760b6828d)`(unsigned int i,unsigned int j,T value)` 

Set value of a component at give indices.

#### Parameters
* `i` column index 

* `j` row index 

* `value` value to set

#### `public inline std::string `[`toString`](#classMatrix_1a6a862549622c26523ad3a9d5263e0574)`()` 

Get string representation of the matrix.

#### Returns
std::string string representation of the matrix

#### `public inline bool `[`operator==`](#classMatrix_1ab1a7748c0ac6dbf3f63c927f9db783a9)`(const `[`Matrix`](#classMatrix)` & other) const` 

Compare two matrices.

#### Parameters
* `other` matrix 

#### Returns
true if the two matrices are equal 

#### Returns
false else

#### `public inline bool `[`operator!=`](#classMatrix_1af0b16242406da9f8a849ea8661f58e73)`(const `[`Matrix`](#classMatrix)` & other) const` 

Check if two matrices are not equal.

#### Parameters
* `other` matrix 

#### Returns
true if the two matrices are not equal 

#### Returns
false else

#### `public inline `[`Matrix`](#classMatrix)` `[`operator=`](#classMatrix_1a39d1492eed5d4334f3056553f7196c62)`(const `[`Matrix`](#classMatrix)` & source)` 

Assign a matrix to another matrix.

#### Parameters
* `source` matrix 

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`operator+`](#classMatrix_1a9cc64309ac456e4dc948eedee3263e32)`(const `[`Matrix`](#classMatrix)` & other)` 

Add two matrices.

#### Parameters
* `other` matrix 

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`operator+`](#classMatrix_1a26494989cf1ab504cacb48ab6115bfef)`(const T scalar)` 

Add a scalar to a matrix.

#### Parameters
* `scalar` 

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`operator-`](#classMatrix_1a1ccb00bdfd32d7907e0601dcd852abb4)`(const `[`Matrix`](#classMatrix)` & other)` 

Subtract two matrices.

#### Parameters
* `other` 

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`operator-`](#classMatrix_1a561ebf4355f56fd22891dd15eac2edbd)`(const T scalar)` 

Subtract a scalar from a matrix.

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`operator/`](#classMatrix_1ae69b53b5925bb653c6916a7fbc42cc69)`(const T scalar)` 

Divide a matrix by a scalar.

#### Parameters
* `scalar` 

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`operator*`](#classMatrix_1a7b1da650e3d336cebea80b32aa89d4e1)`(const `[`Matrix`](#classMatrix)` & other)` 

Multiply two matrices.

#### Parameters
* `other` 

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`operator*`](#classMatrix_1a31550583c457a4334e667be98e391e15)`(const T scalar)` 

Scale a matrix by a scalar.

#### Parameters
* `scalar` number to scale by 

#### Returns
[Matrix](#classMatrix) scaled matrix

#### `public inline void `[`scale`](#classMatrix_1a1cfcb8902f2ffee566a80737b1df976a)`(T scalar)` 

Scale the matrix by a scalar.

#### Parameters
* `scalar`

#### `public inline void `[`apply`](#classMatrix_1a388b917cbc5e2c1549f3ae70bcd70b55)`(T(* T)` 

Apply a function for each component of the matrix

#### Parameters
* `function` The function to apply

#### `public inline `[`Matrix`](#classMatrix)` `[`transpose`](#classMatrix_1a0909615f00692428503e3b9357e22649)`()` 

Get the transpose of the matrix.

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`minor`](#classMatrix_1a0bc7a1822815573004c80d99b7c3fcdd)`(unsigned int a,unsigned int b)` 

Get the minor matrix.

#### Parameters
* `a` column index to remove 

* `b` row index to remove 

#### Returns
minor matrix

#### `public inline `[`Matrix`](#classMatrix)` `[`cofactor`](#classMatrix_1a422a7e24e38811a6eb8d88872cae92b8)`()` 

Compute the cofactor of the matrix.

#### Parameters
* `m` [Matrix](#classMatrix) to compute the cofactor of 

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`adjoint`](#classMatrix_1a495442a4403135e9983b5339366c98e9)`()` 

Compute the adjoin of the matrix.

#### Returns
[Matrix](#classMatrix)

#### `public inline `[`Matrix`](#classMatrix)` `[`inv`](#classMatrix_1a153f654e832e957067d1732c3e234f06)`()` 

Inverse of a matrix.

#### Returns
[Matrix](#classMatrix)

#### `public inline T `[`determinant`](#classMatrix_1a13c1e1219da7b07cb0655c7a5dfaa88e)`()` 

Compute the determinant of the matrix using cofactor method.

#### Returns
the determinant of the matrix

#### `public inline `[`Matrix`](#classMatrix)` `[`copy`](#classMatrix_1a215ee9de53b8648447755ed0b74f40ce)`()` 

Get a copy of the matrix.

#### Returns
[Matrix](#classMatrix)

Generated by [Moxygen](https://sourcey.com/moxygen)