#!/bin/sh
echo "Generate documentation";
doxygen Doxyfile
cd doc
moxygen ./doxy/xml
mv api.md ./algae/docs/api.md
cd ./algae
mkdocs build
echo "Done";
