#pragma once

#include <string>
#include <math.h>

template <typename T> class Matrix {
    public:
	/**
	 * @brief Construct a new Matrix object (empty)
	 * 
	 */
	Matrix()
	{
		m_m = m_n = 0;
		m_M = nullptr;
	}

	/**
	 * @brief Construct a new Matrix object
	 * 
	 * @param m number of rows
	 * @param n number of columns
	 */
	Matrix(unsigned int m, unsigned int n)
	{
		m_m = m; // rows
		m_n = n; // columns
		// Allocate memory
		m_M = (T **) new T *[m];
		for (unsigned int i = 0; i < m_m; i++) {
			m_M[i] = (T *) new T[m_n];
		}
		// Fill with zeros
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				m_M[i][j] = 0;
			}
		}
	}

	/**
	 * @brief Construct a new Matrix object from a 2D array
	 * 
	 * @param data 2D array of type T
	 * @param n number of rows
	 * @param m number of columns
	 * @param byrow specify if the data is given by row or by column
	 */
	Matrix(T *data, unsigned int n, unsigned int m, bool byrow = true)
	{
		m_m = m;
		m_n = n;
		m_M = (T **) new T *[m_m];
		for (unsigned int i = 0; i < m_m; i++) {
			m_M[i] = (T *) new T[m_n];
		}
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				unsigned int index;
				if (byrow) {
					index = i * m_n + j;
				} else {
					index = j * m_m + i;
				}
				m_M[i][j] = data[index];
			}
		}
	}

	/**
	 * @brief Construct a new Matrix object from an other matrix
	 * 
	 * @param source Matrix to copy
	 */
	Matrix(const Matrix &source)
	{
		m_m = source.m_m;
		m_n = source.m_n;
		// Allocate memory
		m_M = (T **)new T *[m_m];
		for (unsigned int i = 0; i < m_m; i++) {
			m_M[i] = (T *)new T[m_n];
		}
		// Fill with values
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				m_M[i][j] = source.m_M[i][j];
			}
		}
	}

	/**
	 * @brief Destroy the Matrix object
	 * 
	 */
	~Matrix()
	{
		if (m_n > 0) {
			for (unsigned int i = 0; i < m_m; i++) {
				delete[] m_M[i];
			}
		}
		if (m_m > 0) {
			delete[] m_M;
		}
	}

	/**
	 * @brief Get matrix component at given indices
	 * @param i column index
	 * @param j row index
	 * @return T value at given indices
	 */
	T at(unsigned int i, unsigned int j)
	{
		if (j >= m_m || i >= m_n)
			return;
		return m_M[j][i];
	}

	/**
	 * @brief Get the row of the matrix at given index
	 * 
	 * @param j row index
	 * @return T* row at given index
	 */
	T *row(unsigned int j)
	{
		T *r = new T[m_n];
		for (unsigned int i = 0; i < m_n; i++) {
			r[i] = m_M[j][i];
		}
		return r;
	}

	/**
	 * @brief Get the column of the matrix at given index
	 * 
	 * @param i column index
	 * @return T* column at given index
	 */
	T *col(unsigned int i)
	{
		T *c = new T[m_m];
		for (unsigned int j = 0; j < m_m; j++) {
			c[j] = m_M[j][i];
		}
		return c;
	}

	/**
	 * @brief Set value of a component at give indices
	 * 
	 * @param i column index
	 * @param j row index
	 * @param value value to set
	 */
	void set(unsigned int i, unsigned int j, T value)
	{
		if (i >= m_m || j >= m_n)
			throw "Algae Error: Matrix index out of bounds";	
		else 
			m_M[j][i] = value;
	}

	/**
	 * @brief Get string representation of the matrix
	 * 
	 * @return std::string string representation of the matrix
	 */
	std::string toString()
	{
		std::string repr = "";
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				repr += std::to_string(m_M[i][j]) + " ";
			}
			repr += "\n";
		}
		return repr;
	}

	/**
	 * @brief Compare two matrices
	 * 
	 * @param other matrix
	 * @return true if the two matrices are equal
	 * @return false else
	 */
	bool operator==(const Matrix &other) const
	{
		// Check dimensions
		if (other.m_m != m_m || other.m_n != m_n) {
			return false;
		} else {
			// Check if all values are the same
			for (unsigned int i = 0; i < m_m; i++) {
				for (unsigned int j = 0; j < m_n; j++) {
					if (m_M[i][j] != other.m_M[i][j])
						return false;
				}
			}
		}
		return true;
	}

	/**
	 * @brief Check if two matrices are not equal
	 * 
	 * @param other matrix
	 * @return true if the two matrices are not equal
	 * @return false else
	 */
	bool operator!=(const Matrix &other) const
	{
		return !(*this == other);
	}

	/**
	 * @brief Assign a matrix to another matrix
	 * 
	 * @param source matrix 
	 * @return Matrix 
	 */
	Matrix operator=(const Matrix &source)
	{
		// Free previously allocated memory for this
		if (m_n > 0) {
			for (unsigned int i = 0; i < m_m; i++) {
				delete[] m_M[i];
			}
		}
		if (m_m > 0) {
			delete[] m_M;
		}
		// Copy data from source
		m_m = source.m_m;
		m_n = source.m_n;
		m_M = (T **) new T *[m_m];
		for (unsigned int i = 0; i < m_m; i++) {
			m_M[i] = (T *) new T[m_n];
			for (unsigned int j = 0; j < m_n; j++) {
				m_M[i][j] = source.m_M[i][j];
			}
		}
		return *this;
	}

	/**
	 * @brief Add two matrices
	 * 
	 * @param other matrix
	 * @return Matrix 
	 */
	Matrix operator+(const Matrix &other)
	{
		if (other.m_m != m_m || other.m_n != m_n) {
			throw "Algae Error: Cannot add two different sized matricies.";
		} else {
			Matrix<T> m(m_m, m_n);
			for (unsigned int i = 0; i < m_m; i++) {
				for (unsigned int j = 0; j < m_n; j++) {
					T addition =
						m_M[i][j] + other.m_M[i][j];
					m.set(i, j, addition);
				}
			}
			return m;
		}
	}

	/**
	 * @brief Add a scalar to a matrix
	 * 
	 * @param scalar 
	 * @return Matrix 
	 */
	Matrix operator+(const T scalar)
	{
		Matrix<T> m(*this);
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				m.m_M[i][j] += scalar;
			}
		}
		return m;
	}

	/**
	 * @brief Subtract two matrices
	 * 
	 * @param other 
	 * @return Matrix 
	 */
	Matrix operator-(const Matrix &other)
	{
		if (other.m_m != m_m || other.m_n != m_n) {
			throw "Algae Error: Cannot substract two different sized matricies.";
		} else {
			Matrix<T> m(m_m, m_n);
			for (unsigned int i = 0; i < m_m; i++) {
				for (unsigned int j = 0; j < m_n; j++) {
					T substraction =
						m_M[i][j] - other.m_M[i][j];
					m.set(i, j, substraction);
				}
			}
			return m;
		}
	}

	/**
	 * @brief Subtract a scalar from a matrix
	 * @return Matrix
	 */
	Matrix operator-(const T scalar)
	{
		Matrix<T> m(*this);
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				m.m_M[i][j] -= scalar;
			}
		}
		return m;
	}

	/**
	 * @brief Divide a matrix by a scalar
	 * 
	 * @param scalar 
	 * @return Matrix 
	 */
	Matrix operator/(const T scalar)
	{
		if (scalar == 0) {
			throw "Algae Error: Cannot divide matrix by zero.";
		} else {
			Matrix<T> m(m_m, m_n);
			for (unsigned int i = 0; i < m_m; i++) {
				for (unsigned int j = 0; j < m_n; j++) {
					T division = m_M[i][j] / scalar;
					m.set(i, j, division);
				}
			}
			return m;
		}
	}

	/**
	 * @brief Multiply two matrices
	 * 
	 * @param other 
	 * @return Matrix 
	 */
	Matrix operator*(const Matrix &other)
	{
		if (m_n != other.m_m) {
			throw "Algae Error: Cannot mutliply matrix with a number of column different that number of row of the other matrix";
		} else {
			Matrix<T> m(m_m, m_n);
			for (unsigned int i = 0; i < m_m; i++) {
				for (unsigned int j = 0; j < other.m_n; j++) {
					m.m_M[i][j] = 0;
					for (unsigned int k = 0; k < m_n; k++) {
						m.m_M[i][j] += m_M[i][k] *
							       other.m_M[k][j];
					}
				}
			}
			return m;
		}
	}
	
	/**
	 * @brief Scale a matrix by a scalar
	 * 
	 * @param scalar number to scale by
	 * @return Matrix scaled matrix
	 */
	Matrix operator*(const T scalar)
	{
		Matrix<T> m(m_m, m_n);
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				T multiplication = m_M[i][j] * scalar;
				m.set(i, j, multiplication);
			}
		}
		return m;
	}

	/**
	 * @brief Scale the matrix by a scalar
	 * 
	 * @param scalar 
	 */
	void scale(T scalar)
	{
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				m_M[i][j] *= scalar;
			}
		}
	}

	/**
	 * @brief Apply a function for each component of the matrix	
	 * @param function The function to apply
	 */
	void apply(T (*function(T)))
	{
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				m_M[i][j] = function(m_M[i][j]);
			}
		}
	}

	/**
	 * @brief Get the transpose of the matrix
	 * 
	 * @return Matrix 
	 */
	Matrix transpose()
	{
		Matrix transposed(m_n, m_m);
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				transposed.m_M[j][i] = m_M[i][j];
			}
		}
		return transposed;
	}

	/**
	 * @brief Get the minor matrix
	 * 
	 * @param a column index to remove
	 * @param b row index to remove
	 * @return minor matrix
	 */
	Matrix minor(unsigned int a, unsigned int b) {
		if (b >= m_m || a >= m_n) {
			throw "Algae Error: Cannot get minor of matrix with index out of bounds.";
		}
		if (m_m < 2 || m_n < 2) {
			throw "Algae Error: Cannot get minor of matrix with less than 2 rows or columns.";
		}
		Matrix<T> minor(m_m - 1, m_n - 1);
		for (unsigned int j = 0; j < m_m - 1; j++) {
			for (unsigned int i = 0; i < m_n - 1; i++) {
				unsigned int source_i = i < a ? i : i + 1;
				unsigned int source_j = j < b ? j : j + 1;
				T component = m_M[source_j][source_i];
				minor.set(i, j, component);
			}
		}
		return minor;
	}

	/**
	 * @brief Compute the cofactor of the matrix
	 * 
	 * @param m Matrix to compute the cofactor of
	 * @return Matrix 
	 */
	Matrix cofactor()
	{
		Matrix c(m_m, m_n);
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				Matrix<T> m = minor(i, j);
				c.set(i, j, pow(-1, i + j) * m.determinant());
			}
		}
		return c;
	}

	/**
	 * @brief Compute the adjoin of the matrix
	 * 
	 * @return Matrix 
	 */
	Matrix adjoint()
	{
		return cofactor().transpose();
	}

	/**
	 * @brief Inverse of a matrix
	 * 
	 * @return Matrix 
	 */
	Matrix inv() 
	{
		if (m_m != m_n) {
			throw "Algae Error: Cannot invert a non-square matrix.";
		} else {
			T det = determinant();
			if (det == 0) {
				throw "Algae Error: Cannot invert a matrix with a determinant of 0.";
			} else {
				Matrix cofactors(m_m, m_n);
				Matrix transposed = transpose();
				for (unsigned int i = 0; i < m_m; i++) {
					for (unsigned int j = 0; j < m_n; j++) {
						Matrix submatrix = transposed.minor(i, j);
						T submatrix_det = submatrix.determinant();
						cofactors.set(i, j, toggle(i+j) * (submatrix_det / det));
					}
				}
				return cofactors;
			}
		}
	}

	/**
	 * @brief get (-1)^i
	 * 
	 */
	static int toggle(unsigned int i) 
	{
		return (i % 2) ? -1 : 1;
	}

	/**
	 * @brief Compute the determinant of the matrix using cofactor method
	 * 
	 * @return the determinant of the matrix 
	 */
	T determinant()
	{
		if (m_m != m_n)
			throw "Algae Error: Cannot compute the determinant of a non-square matrix.";
		if (m_m == 1) {
			return m_M[0][0];
		} else if (m_m == 2 && m_n == 2) {
			return m_M[0][0] * m_M[1][1] - m_M[0][1] * m_M[1][0];
		} else {
			T det = 0;
			// Choose the first row
			for (unsigned int i = 0; i < m_n; i++) {
				Matrix submatrix = minor(i, 0);
				det += toggle(i) * m_M[0][i] * submatrix.determinant();
			}
			return det;
		}
	}

	/**
	 * @brief Construct the identity matrix
	 * 
	 * @param size 
	 * @return Matrix 
	 */
	static Matrix identity(unsigned int size)
	{
		Matrix<T> id(size, size);
		for (unsigned int i = 0; i < size; i++) {
			for (unsigned int j = 0; j < size; j++) {
				if (i == j) {
					id.m_M[i][j] = 1;
				} else {
					id.m_M[i][j] = 0;
				}
			}
		}
		return id;
	}

	/**
	 * @brief Get a copy of the matrix
	 * 
	 * @return Matrix 
	 */
	Matrix copy() 
	{
		Matrix m(m_m, m_n);
		// deep copy
		for (unsigned int i = 0; i < m_m; i++) {
			for (unsigned int j = 0; j < m_n; j++) {
				m.set(i, j, m_M[i][j]);
			}
		}
		return m;
	}

    private:
	T **m_M;
	unsigned int m_m; //  number of rows
	unsigned int m_n; // number of columns
};