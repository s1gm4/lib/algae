#pragma once

#include <cmath>
#include <cstdarg>
#include <stdexcept>
#include <string>
#include <vector>
#include <math.h>

template <typename T> class Vector {
    public:
	Vector<T>(unsigned int num, ...)
	{
		va_list args;
		unsigned int i;
		va_start(args, num);
		for (i = 0; i < num; i++) {
			components.push_back(va_arg(args, T));
		}
		va_end(args);
	}

	Vector<T>(Vector<T> &source)
	{
		*this = source;
	}

	void set(unsigned int num, ...)
	{
		va_list args;
		unsigned int i;
		components.clear();
		va_start(args, num);
		for (i = 0; i < num; i++) {
			components.push_back(va_arg(args, T));
		}
		va_end(args);
	}

	T at(unsigned int i)
	{
		return components.at(i);
	}

	void add(Vector other)
	{
		if (other.components.size() != components.size()) {
			throw std::length_error(
				"Algae Error: Cannot add two vector of different size.");
		} else {
			for (unsigned int i = 0; i < components.size(); i++) {
				components.at(i) += other.components.at(i);
			}
		}
	}

	static Vector add(Vector one, Vector two)
	{
		Vector addition = one;
		addition.add(two);
		return addition;
	}

	void sub(Vector other)
	{
		if (other.components.size() != components.size()) {
			throw std::length_error(
				"Algae Error: Cannot subtract two different sized vectors.");
		} else {
			for (unsigned int i = 0; i < components.size(); i++) {
				components.at(i) -= other.components.at(i);
			}
		}
	}

	static Vector sub(Vector one, Vector two)
	{
		Vector v(one);
		v.sub(two);
		return v;
	}

	void mul(Vector other)
	{
		if (other.components.size() != components.size()) {
			throw std::length_error(
				"Algae Error: Cannot multiply two different sized vectors.");
		} else {
			for (unsigned int i = 0; i < components.size(); i++) {
				components.at(i) *= other.components.at(i);
			}
		}
	}
	static Vector mul(Vector one, Vector two)
	{
		Vector v(one);
		v.mul(two);
		return v;
	}

	void div(Vector other)
	{
		if (other.components.size() != components.size()) {
			throw std::length_error(
				"Algae Error: Cannot subtract two different sized vectors.");
		} else {
			for (unsigned int i = 0; i < components.size(); i++) {
				T divisor = other.components.at(i);
				if (divisor == 0) {
					throw std::overflow_error(
						"Algae Error: Cannot divide by zero the component of the vector.");
				}
				components.at(i) -= other.components.at(i);
			}
		}
	}

	static Vector div(Vector one, Vector other)
	{
		Vector v(one);
		v.div(other);
		return v;
	}

	T dot(Vector other)
	{
		if (other.components.size() != components.size()) {
			throw std::length_error(
				"Algae Error: Cannot compute dot product of two different sized vectors.");
		} else {
			T d = 0;
			for (unsigned int i = 0; i < components.size(); i++) {
				d += components.at(i) * other.components.at(i);
			}
			return d;
		}
	}

	static T dot(Vector one, Vector other)
	{
		return one.dot(other);
	}

	void scale(T scalar)
	{
		for (unsigned int i = 0; i < components.size(); i++) {
			components.at(i) *= scalar;
		}
	}

	T length() {
		T sum = 0;
		for (unsigned int i = 0; i < components.size(); i++) {
			sum += components.at(i);
		}
		return std::sqrt(sum);
	}

	void normalize()
	{
		scale(1 / length());
	}

	void set_mag(T mag) 
	{
		normalize();
		scale(mag);
	}

	void limit(T mag)
	{
		if (length() > mag) {
			set_mag(mag);
		}
	}

	float angle(Vector other)
	{
		return std::acos(dot(other) / (length() * other.length()));
	}

	Vector cross(Vector<T> other)
	{
		if (components.size() != 3 || other.components.size() != 3) {
			throw std::length_error(
				"Algae Error: Cannot perform cross product on not three dimensional vectors.");
		} else {
			return Vector<T>(
				3,
				components.at(1) * other.components.at(2) -
					components.at(2) *
						other.components.at(1),
				components.at(2) * other.components.at(0) -
					components.at(0) *
						other.components.at(2),
				components.at(0) * other.components.at(1) -
					components.at(1) *
						other.components.at(0));
		}
	}

	static T cross(Vector<T> one, Vector<T> other)
	{
		return one.cross(other);
	}

	void negate()
	{
		scale(-1);
	}

	bool operator==(const Vector &other) const
	{
		if (other.components.size() != components.size()) {
			return false;
		} else {
			for (unsigned int i = 0; i < components.size(); i++) {
				if (components.at(i) != other.components.at(i))
					return false;
			}
		}
		return true;
	}

	bool operator!=(const Vector &other) const
	{
		return !(*this == other);
	}

	Vector operator=(const Vector &source)
	{
		components.clear();
		for (unsigned int i = 0; i < source.components.size(); i++) {
			components.push_back(source.components.at(i));
		}
	}

	std::string toString()
	{
		std::string repr = "(";
		for (unsigned int i = 0; i < components.size() - 1; i++) {
			repr += std::to_string(components.at(i)) + ", ";
		}
		if (components.size() > 0) {
			repr += std::to_string(
				components.at(components.size() - 1));
		}
		repr += ")";
		return repr;
	}

    private:
	std::vector<T> components;
};